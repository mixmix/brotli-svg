const brotli = require('brotli')
const fs = require('fs')
const { join } = require('path')

const file = fs.readFileSync(join(__dirname, 'src/mix.svg'))

const result = brotli.compress(file, {
  mode: 1, // 0 = generic, 1 = text, 2 = font (WOFF2)
  quality: 11, // 0 - 11
  lgwin: 22 // window size
})

fs.writeFileSync(join(__dirname, 'public/mix.svg.br'), result)
