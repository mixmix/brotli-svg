const express = require('express')
const expressStaticGzip = require('express-static-gzip')
const { join } = require('path')

const app = express()

app.use('/', expressStaticGzip(join(__dirname, 'public'), {
  enableBrotli: true,
  orderPreference: ['br', 'gz']
}))

app.listen(4000)

console.log('http://localhost:4000/mix.svg')
