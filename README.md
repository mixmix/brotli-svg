Made an SVG with https://www.samcodes.co.uk/project/geometrize-haxe-web/

Compressed it with brotli 82kb -> 7kb

```
node compress.js
```

Serve it 

```
node server.js
```

load the image at localhost:4000/mix.svg it in the browser... open the dev-tools!

![](dev-tools.png)

